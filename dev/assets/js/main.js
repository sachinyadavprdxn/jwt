$(document).ready(function() {

	$( "nav ul li" ).on( "mouseover", function() {
		$( this ).find( "span" ).addClass( "bottom-arrow" );
	});

	$( "nav ul li" ).on( "mouseout", function() {
		$( "nav ul li span" ).removeClass( "bottom-arrow" );
	});

// ****** Styling for Accordion ******* //
	$( ".accordion ul li" ).on( "click", function() {
		$(this).toggleClass("active");
    $(this).siblings().removeClass("active");

		$( ".accordion span.plus" ).removeClass( "accordion-minus" );
		$( ".accordion .active span.plus" ).toggleClass( "accordion-minus" );

		$( ".accordion ul li p" ).removeClass( "accContent" );
		$( ".accordion .active p" ).toggleClass( "accContent" );
	});

// ****** Styling for Jobsteps ******* //
	$("#tab2, #tab3, #tab4").css("display", "none");

	$( "ul.steptabs li" ).on( "click", function() {
		$("ul.steptabs li a").addClass("active");
		$(this).siblings().find("a").removeClass("active");
		var attr_value = $( this ).find( "a" ).attr("href");
		$(attr_value).css("display", "block");
		$(attr_value).siblings("div").css("display", "none");
	});

// ****** Calculating height of moduls and middle content ******* //
	var content_hyt = $( ".middle-content" ).height();
	var moduals_hyt = $( ".moduals" ).height();
	$(".middle-content > div").css("height", content_hyt);
	$(".modual").css("height", moduals_hyt);

});

